$.when($.ready).then(function() {

    var str = navigator.userAgent;
    var checkUser = 0;
    var mic = 0;
    var vol = 0;
    var cont = 0;
    var audioContext = null;
    var meter = null;
    var mediaStreamSource = null;
    var contValue = 0;
    var contCheck = 0;


    function isMobileDevice() {
        return (typeof window.orientation !== "undefined") || (navigator.userAgent.indexOf('IEMobile') !== -1);
    };

    function initAudioContext() {

        // Older browsers might not implement mediaDevices at all, so we set an empty object first
        if (navigator.mediaDevices === undefined) {
          navigator.mediaDevices = {};
        }

        // Some browsers partially implement mediaDevices. We can't just assign an object
        // with getUserMedia as it would overwrite existing properties.
        // Here, we will just add the getUserMedia property if it's missing.
        if (navigator.mediaDevices.getUserMedia === undefined) {
            navigator.mediaDevices.getUserMedia = function(constraints) {

            // First get ahold of the legacy getUserMedia, if present
            var getUserMedia = navigator.webkitGetUserMedia || navigator.mozGetUserMedia;

            // Some browsers just don't implement it - return a rejected promise with an error
            // to keep a consistent interface
            if (!getUserMedia) {
              return Promise.reject(new Error('getUserMedia is not implemented in this browser'));
            }

            // Otherwise, wrap the call to the old navigator.getUserMedia with a Promise
            return new Promise(function(resolve, reject) {
              getUserMedia.call(navigator, constraints, resolve, reject);
            });
          }
        }

        if (checkUser != 1) {

            navigator.mediaDevices.getUserMedia({ audio: true, video: false })
            .then(function(stream) {

                // First, get the AudioContext from MediaStreamSource;
                window.AudioContext = window.AudioContext || window.webkitAudioContext;
                audioContext = new AudioContext();

                gotStream(stream);
                mic = 1;

            })
            .catch(function(err) {
                gotStream(null);
            });

        } else {

            gotStream(null);

        }

    }

    function gotStream(stream) {

        if (stream != null) {

            mediaStreamSource = audioContext.createMediaStreamSource(stream);
            meter = createAudioMeter(audioContext);
            mediaStreamSource.connect(meter);

        }

        draw();

    }

    var sec = 0;

    function draw(timestamp) {      

        if (mic == 0) {
            
            var obj = $('.console p').text('getUserMedia is not implemented in this browser');
            obj.html(obj.html().replace(/\n/g,'<br/>'));
            
        } else {

            var value = meter.volume;

            if (isMobileDevice()) {
                value = meter.volume * 2;
            }

            //So, after many tests, I define that the values obtained
            //below 0.01 of 1 integer are discarded, keeping a minimum limit
            //for the "value" variable, just to try to stabilize the calculation
            //and avoid these infinite returns.
            if (value < 0.01) {
                value = 0.01;
            }

            // Try to reproduce what is done when is create an app properly using Java.
            // In this case, a method called getMaxAmplitude returns a value between 0 and 32767.
            // I calculate a rule of 3 to calculate the correction factor.
            var converted_value = ((value * 32767)/1);

            //var calculate_db = 10 * converted_value / 32767;
            // Calculate the dB using the standard formula
            var calculate_db = 20 * Math.log10(converted_value);

        }

        // Calculate the average
              

        if (sec > 12) {

            if (meter.volume > 0) {
                contValue += calculate_db;
                contCheck++;
            }  
            
            var obj = $('.console p').text(
                '\nconverted_value - ' + Math.floor(converted_value) + ' of 32767' +
                '\ncalculate_db - ' + Math.floor(calculate_db) + 
                '\naverage -  ' +  Math.floor(contValue/contCheck) + 
                '\nvalue - ' + value + 
                '\nmeter.volume - ' + meter.volume);
            obj.html(obj.html().replace(/\n/g,'<br/>'));
            sec = 0;

        } else {
            sec++;
        }

        requestAnimationFrame(draw);

    }

    $('.btn').click(function() {

        $(this).remove();
        initAudioContext();

    });

    function createAudioMeter(audioContext, clipLevel, averaging, clipLag) {

        var processor = audioContext.createScriptProcessor(4096,1,1);
        processor.onaudioprocess = volumeAudioProcess;
        processor.clipping = false;
        processor.lastClip = 0;
        processor.volume = 0;
        processor.clipLevel = clipLevel || 0.98;
        processor.averaging = averaging || 0.95;
        processor.clipLag = clipLag || 750;
        processor.connect(audioContext.destination);

        return processor;
    }

    function volumeAudioProcess(event) {

        var buf = event.inputBuffer.getChannelData(0);
        var bufLength = buf.length;
        var sum = 0;

        for (var i = 0; i < buf.length; ++i) {
          sum += buf[i] * buf[i];
        }
        var vol = Math.sqrt(sum / buf.length);

        // Smooth this out with the averaging factor
        // Calculate the device volume using inputBuffer from Channel data (value is between 0 to 1)
        this.volume = Math.max(vol, this.volume * this.averaging);

    }

});

