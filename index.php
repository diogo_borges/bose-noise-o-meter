<?php
    $date = microtime(true)*1000;
?>
<html>
<head>

	<meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>BOSE - Noise-O-Meter</title>
    <meta name="keywords" content="wireless,android headphones,best-sounding headphones,noise reducing,headphones 700,bose noise cancelling headphones,smart headphones,over ear headphones,wireless noise cancelling headphones,touch control,wireless headphones,noise cancelling headphones,iphone headphones,headphones,wire-free,Bose AR,best performance,bluetooth headphones,Google assistant headphones,amazon alexa headphones,best noise cancelling,apple headphones,noise cancellation,around-ear">
	
	<meta name="description" content="The smart Bose Noise Cancelling Headphones 700 are our most elegant yet. With accurate hands-free voice control and next-gen active noise reduction. Charging case included.">
	<!--FB-->
	<meta property="og:type" content="product"/>
	<meta property="og:title" content="Noise-O-Meter | Bose">
	<meta property="og:description" content="The smart Bose Noise Cancelling Headphones 700 are our most elegant yet. With accurate hands-free voice control and next-gen active noise reduction. Charging case included."/>
	<meta property="og:image" content="https://assets.bose.com/content/dam/Bose_DAM/Web/consumer_electronics/global/products/headphones/noise_cancelling_headphones_700/fuji_kit/product_silo_images/noise_cancelling_headphones_700_Fuji_EC_hero.psd/_jcr_content/renditions/cq5dam.web.320.320.png"/>
	<meta property="og:url" content="https://www.bose.ae/noise-o-meter">
	<!--Twitter-->
	<meta name="twitter:card" content="summary">
	<meta name="twitter:site" content="@Bose">
	<meta property="twitter:title" content="Noise-O-Meter | Bose">
	<meta property="twitter:description" content="The smart Bose Noise Cancelling Headphones 700 are our most elegant yet. With accurate hands-free voice control and next-gen active noise reduction. Charging case included."/>
	<meta property="twitter:image" content="https://assets.bose.com/content/dam/Bose_DAM/Web/consumer_electronics/global/products/headphones/noise_cancelling_headphones_700/fuji_kit/product_silo_images/noise_cancelling_headphones_700_Fuji_EC_hero.psd/_jcr_content/renditions/cq5dam.web.320.320.png"/>

    <link rel="shortcut icon" href="favicon.ico">

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
	<link rel="stylesheet" href="css/style.css?v=<?php echo $date ?>">

</head>
<body>

	<div class="soundcheck">

		<div class="console">
			<p style="color: #fff;"></p>
		</div>

		<div class="btn">
			<span>Check the noise</span>
		</div>
		
	</div>



	<script src='js/jquery.min.js'></script>
	<script src="js/script.js?v=<?php echo $date ?>"></script>

</body>
</html>
